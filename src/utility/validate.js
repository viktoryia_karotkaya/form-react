export const checkValidity = (val, rules) => {
  let isValid = false;

  let requiredRule = (() =>{
    if(rules.required){
      return  val.trim() !=='';
    } else {
      return true;
    }
  })();
  
  let letterRule = (() =>{
    if(rules.onlyLetters){
      const pattern = /^[a-zA-Z\s]*$/;
      return pattern.test(val);
    } else {
      return true;
    }
  })();
  
  let numericRule = (() =>{
    if(rules.onlyNumbers){
      const value = parseInt(val);
      const pattern = /^\d+$/;
      return pattern.test(value);
    } else {
      return true;
    }
  })();
  
  let maxRule = (() =>{
    if(rules.max){
      const value = parseInt(val);
      return value <= rules.max;
    } else {
      return true;
    }
  })();
  
  let maxLengthRule = (() =>{
    if(rules.maxLength){
      return val.length <= rules.maxLength;
    } else {
      return true;
    }
  })();
  
  let whiteSpacesRule = (() =>{
    if(val.length && rules.noWhiteSpaces){
      return  val.trim() !=='';
    } else {
      return true;
    }
  })();
  
  
  isValid = requiredRule && maxLengthRule && numericRule && letterRule && maxRule && whiteSpacesRule;
  return isValid;
};
