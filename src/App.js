import './App.css';
import Form from "./Components/form/Form";
import {initialFormState} from "./data-folder/initial-data";

function App() {
  return (
    <div className="App">
      <Form initialState = {initialFormState} />
    </div>
  );
}

export default App;
