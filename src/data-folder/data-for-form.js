const saucesList = [
  {
    name: "ketchup",
    checked: false
  },
  {
    name: "mustard",
    checked: false
  },
  {
    name: "mayonnaise",
    checked: false
  },
  {
    name: "guacamole",
    checked: false
  }
];

const stoogeList = [
  "larry",
  "moe",
  "curly"
];

const colorsList = [
  {value: "#ff0000ff", label: "Red"},
  {value: "#008000ff", label: "Green"},
  {value: "#0000ffff", label: "Blue"},
  {value: "#ffff00ff", label: "Yellow"}
];

export {saucesList, stoogeList, colorsList};
