export const initialFormState = {
  firstName: {
    initial: '',
    validateRules: {
      required: true,
      onlyLetters: true
    }
  },
  lastName: {
    initial: '',
    validateRules: {
      required: true,
      onlyLetters: true
    }
  },
  age: {
    initial: '',
    validateRules: {
      required: true,
      onlyNumbers: true,
      max: 100
    }
  },
  employed: false,
  favoriteColor: {
    initial: '',
    validateRules: {
      required: true
    }
  },
  stooge: 'larry',
  sauces: [false, false, false, false],
  notes: {
    initial: '',
    validateRules: {
      maxLength: 100,
      noWhiteSpaces: true
    }
  }
};
