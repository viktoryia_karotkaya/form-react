import "./Select.css";
const Select = (props) => {
  return (
    <div className="col-sm-9">
      <select
        className={!props.valid && props.touched ? "form-select invalid" : "form-select"}
        value={props.value}
        name={props.name}
        onChange={e => props.callback(e, props.name)} >
        <option key={0} />
        {props.colorsList.map((item, index) => {
          return (<option key={index+1} value={item.value}>{item.label}</option>)
        })}
      </select>
    </div>
  );
}

export default Select;
