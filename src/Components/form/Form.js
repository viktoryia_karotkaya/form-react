import {useState} from "react";
import "./Form.css";
import {saucesList, stoogeList, colorsList} from "../../data-folder/data-for-form";
import SummaryBox from "../summary-box/SummaryBox";
import RadioInput from "../radio-input/RadioInput";
import Input from "../Input/Input";
import CheckboxInput from "../checkbox-input/CheckboxInput";
import Select from "../select/Select";
import GroupCheckboxes from "../group-checkboxes/GroupCheckboxes";
import Textarea from "../Textarea/Textarea";
import {checkValidity} from "../../utility/validate";
import {initialFormState} from "../../data-folder/initial-data";

const Form = props => {
  const [form, setState] = useState({
    firstName: {value: props.initialState.firstName.initial, valid: false, touched: false},
    lastName: {value: props.initialState.lastName.initial, valid: false, touched: false},
    age: {value: props.initialState.age.initial, valid: false, touched: false},
    employed: props.initialState.employed,
    favoriteColor: {value: props.initialState.favoriteColor.initial, valid: false, touched: false},
    sauces: props.initialState.sauces,
    stooge: props.initialState.stooge,
    notes: {value: props.initialState.notes.initial, valid: true},
  });
 
  const [summaryObj, setSummaryObj] = useState({
    stooge: 'larry',
    employed: false
  });
 
  const handleOnChangeWithValidation = (e, name) => {
    const value = e.target.value;
    const rules = props.initialState[name].validateRules;
    const isValid = checkValidity(value, rules);
    const changedStateObj = {value: value, valid: isValid, touched: true};
    setState({...form, [e.target.name]: changedStateObj, untouched: false});
    setSummaryObj({...summaryObj, [e.target.name]: value});
  }
  
  const handleOnChange = (e) => {
    setState({...form, [e.target.name]: e.target.value});
    setSummaryObj({...summaryObj, [e.target.name]: e.target.value});
  }
  
  const handleOnChangeForSauces = (position) => {
    const updatedSauces = form.sauces.map((item, index) =>
      index === position ? !item : item
    );
    setState({...form, sauces: updatedSauces});
    const updatedSaucesForSummaryObj = [];
    updatedSauces.forEach((item, index) =>
        item ? updatedSaucesForSummaryObj.push(saucesList[index].name) : undefined
    );
    setSummaryObj({...summaryObj, sauces: updatedSaucesForSummaryObj});
  }
  
  const handleOnChangeForEmployed = () => {
    setState({...form, employed: !form.employed});
    setSummaryObj({...summaryObj, employed: !form.employed})
  }
  
  const submitHandler = (event) => {
    event.preventDefault();
    alert(prepareObjectToDisplay());
  };
  
  const prepareObjectToDisplay = () => {
    const data = Object.fromEntries(Object.entries(summaryObj).filter(([_, v]) => {
      if (Array.isArray(v)) {
        return v.length > 0
      } else {
        return v !== ''
      }
    } ));
    return JSON.stringify(data, null, 2);
  };
  
  const resetForm = () => {
    debugger;
    let newForm = {...form};
    for (const field in initialFormState){
      if (typeof newForm[field] === "object" && !Array.isArray(newForm[field]) && newForm[field] !== null){
        let defaultField = {};
        defaultField.value = initialFormState[field].initial;
        if (field === "notes") {
          defaultField.valid = true;
        }  else {
          defaultField.valid = false;
          defaultField.touched = false;
        }
        newForm[field] = defaultField;
      } else {
        newForm[field] = initialFormState[field]
      }
    }
    setState(newForm);
    setSummaryObj({
      stooge: 'larry',
      employed: false
    });
  }
  
  return (
    <form className="Form" onSubmit={submitHandler}>
      <div className="mb-3 row">
        <label htmlFor="firstName" className="col-sm-3 col-form-label">Firs Name:</label>
        <Input
          type="text"
          name="firstName"
          value={form.firstName.value}
          callback={handleOnChangeWithValidation}
          placeholder="First Name"
          valid={form.firstName.valid}
          touched={form.firstName.touched}
        />
      </div>
      <div className="mb-3 row">
        <label htmlFor="lastName" className="col-sm-3 col-form-label">Last Name:</label>
        <Input
          type="text"
          name="lastName"
          value={form.lastName.value}
          callback={handleOnChangeWithValidation}
          placeholder="Last Name"
          valid={form.lastName.valid}
          touched={form.lastName.touched}
        />
      </div>
      <div className="mb-3 row">
        <label htmlFor="age" className="col-sm-3 col-form-label">Age:</label>
          <Input
            type="number"
            name="age"
            value={form.age.value}
            callback={handleOnChangeWithValidation}
            placeholder="Age"
            valid={form.age.valid}
            touched={form.age.touched}
          />
      </div>
      <div className="mb-3 row">
        <label htmlFor="employed" className="col-sm-3 col-form-label">Employed:</label>
        <div className="col-sm-9 employed">
          <CheckboxInput
            name="employed"
            checked={form.employed}
            callback={handleOnChangeForEmployed}
          />
        </div>
      </div>
      <div className="mb-3 row">
        <label htmlFor="favoriteColor" className="col-sm-3 col-form-label">Favorite Color:</label>
        <Select
          name="favoriteColor"
          colorsList={colorsList}
          value={form.favoriteColor.value}
          callback={handleOnChangeWithValidation}
          valid={form.favoriteColor.valid}
          touched={form.favoriteColor.touched}
        />
      </div>
      <div className="mb-3 row">
        <label htmlFor="sauces" className="col-sm-3 col-form-label">Sauces:</label>
        <GroupCheckboxes
          sauces={saucesList}
          name={form.sauces}
          callback={handleOnChangeForSauces}
        />
      </div>
      <div className="mb-3 row">
        <label htmlFor="bestStooge" className="col-sm-3 col-form-label">Best stooge:</label>
        <div className="col-sm-9 form-check">
          {stoogeList.map((item, index) => {
            return <RadioInput
                      key={index}
                      name="stooge"
                      label={item}
                      value={item}
                      checked={form.stooge}
                      callback={handleOnChange}
            />
          })}
        </div>
      </div>
      <div className="mb-3 row">
        <label htmlFor="notes" className="col-sm-3 col-form-label">Notes:</label>
        <div className="col-sm-9">
          <Textarea
            value={form.notes.value}
            name="notes"
            callback={handleOnChangeWithValidation}
            valid={form.notes.valid}
          />
        </div>
      </div>
      <div className="d-flex justify-content-center">
        <input
          className="btn p-2 btn-primary btnSubmit"
          type="submit"
          disabled={
            !form.firstName.valid ||
            !form.lastName.valid ||
            !form.age.valid ||
            !form.favoriteColor.value ||
            !form.notes.valid
          }
          value="Submit"
        />
        <input className="btn p-2 btn-secondary" type="reset" value="Reset" onClick={resetForm} />
      </div>
      <SummaryBox data={prepareObjectToDisplay()} />
    </form>
  );
}

export default Form;
