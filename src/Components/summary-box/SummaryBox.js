import "./SummaryBox.css";

const SummaryBox = (props) => {
  return (
    <div className="SummaryBox">
      <pre >
        <code>
          {props.data}
        </code>
      </pre>
    </div>
  );
}

export default SummaryBox;
