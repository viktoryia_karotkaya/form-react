import "./Textarea.css";
const Textarea = (props) => {
  return (
    <textarea
      className={!props.valid ? "form-control invalid" : "form-control"}
      rows="2"
      name={props.name}
      value={props.value}
      onChange={e => props.callback(e, props.name)}
    />
  );
}

export default Textarea;
