import "./Input.css";

const Input = (props) => {
  return (
      <div className="col-sm-9">
        <input
          className={!props.valid && props.touched ? "form-control invalid" : "form-control"}
          type={props.type}
          name={props.name}
          value={props.value}
          placeholder={props.placeholder}
          onChange={e => props.callback(e, props.name)}
        />
      </div>
  );
};

export default Input;
