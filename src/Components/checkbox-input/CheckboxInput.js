import "./CheckboxInput.css";

const CheckboxInput = (props) => {
  return (
      <input
        className="form-check-input"
        type="checkbox"
        name={props.name}
        checked={props.checked}
        onChange={props.callback}
      />
  );
}

export default CheckboxInput;
