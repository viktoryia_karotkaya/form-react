import CheckboxInput from "../checkbox-input/CheckboxInput";
import "./GroupCheckboxes.css";

const GroupCheckboxes = (props) => {
  return (
    <ul className="col-sm-9 form-check sauces-list">
      {props.sauces.map((item, index) => {
        return (
          <li key={index}>
            <div className="form-check">
              <CheckboxInput
                name={item.name}
                checked={props.name[index]}
                callback={() => props.callback(index)}
              />
              <label className="form-check-label" htmlFor={item.name}>
                {item.name}
              </label>
            </div>
          </li>
        );
      })}
    </ul>
  );
}

export default GroupCheckboxes;
