const RadioInput = (props) => {
  return (
    <div className="form-check">
      <input
        className="form-check-input"
        type="radio"
        name={props.name}
        value={props.value}
        checked={props.checked === props.value}
        onChange={e => props.callback(e)} />
      <label className="form-check-label" htmlFor="larry">
        {props.label}
      </label>
    </div>
  );
};

export default RadioInput;
